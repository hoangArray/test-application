//
//  LogicModel.swift
//  TestApp
//
//  Created by Anton Hoang on 08/04/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation

class Counter {
    
    //Find palindrome
    func findPalindrome (number : Int) -> Bool {
        //Create local variable that we can assign to it
        var inputValue = number
        
        //Create reverse number (will begin from the right side)
        var reversedNumber = 0
        
        while inputValue > 0 {
            
            //Now get single digit of input number
            let rightDigit = inputValue % 10
            
            //Assign right digit into end of RIGHT side in reversedNumber
            reversedNumber = reversedNumber * 10 + rightDigit
            
            //Get single digit by split on decimal number
            inputValue = inputValue / 10
        }
        //The number will palindrome if they are equal from each sides (left and right) return true
        return number == reversedNumber
    }
    
    //Find sum of factorial
    func multiply (numberContainer : inout [Int], x : Int) {
        
        var carry = 0
        let size = numberContainer.count
        
        for i in 0..<size {
            
            let res = carry + numberContainer[i] * x
            
            numberContainer[i] = res % 10
            carry = res/10
        }
        while (carry != 0) {
            numberContainer.append(carry % 10)
            carry /= 10
        }
    }
    
    func findSumOfDigits (n : Int) -> Int {
        
        var numberContainer : [Int] = []
        numberContainer.append(1)
        
        for i in 1...n {
            multiply(numberContainer: &numberContainer, x: i)
        }
    
        var sum = 0
        let size = numberContainer.count
        
        for i in 0..<size {
            sum += numberContainer[i]
        }
        
        print(sum)
        return sum
    }
}

struct Pairs : Comparable {
    
    let firstElement: Int
    let secondElement: Int
    
    static func < (lrh : Pairs , rhs : Pairs) -> Bool {
        let leftDistance = lrh.firstElement - lrh.secondElement
        let rightDistance = rhs.firstElement - rhs.secondElement
        
        if leftDistance != rightDistance {
            return leftDistance < rightDistance
        }
        return lrh.secondElement > rhs.secondElement
    }
    
    static func > (lrh : Pairs , rhs : Pairs) -> Bool {
        let leftDistance = lrh.firstElement - lrh.secondElement
        let rightDistance = rhs.firstElement - rhs.secondElement
        
        if leftDistance != rightDistance {
            return leftDistance > rightDistance
        }
        return lrh.secondElement > rhs.secondElement
    }
}

extension Array where Element : Comparable {
    
    func getLongestSublist () -> (Element?, Element?) {
        
        var min = self.first
        var max = self.first
        
        for i in self {
            if i < min! {
                min = i
            }
            if i > max! {
                max = i
            }
        }
        return (min, max)
    }
}



