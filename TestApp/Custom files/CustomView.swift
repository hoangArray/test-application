//
//  CustomTextField.swift
//  TestApp
//
//  Created by Anton Hoang on 15/04/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit

//SuperClass
class CustomView: UIView {
    
    @IBOutlet var customView: UIView!

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textField: TextFieldView!
    @IBOutlet weak var calculateButton: UIButton!
    @IBOutlet weak var resultLabel: UILabel!

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit () {
        Bundle.main.loadNibNamed("CustomTextField", owner: self, options: nil)
        addSubview(customView)
        customView.frame = self.bounds
        customView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        textField.bottomLine.frame = CGRect(x: 0,
                                  y: self.frame.size.height - textField.bottomLine.borderWidth,
                                  width: self.frame.size.width,
                                  height: textField.bottomLine.borderWidth)
        
        textField.bottomLine.borderWidth = 2
        textField.borderStyle = .none
        textField.bottomLine.borderColor = UIColor.red.cgColor
    
    }
}
