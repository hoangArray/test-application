//
//  CustomButton.swift
//  TestApp
//
//  Created by Anton Hoang on 12/04/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit

class CustomButton: UIView {

    @IBOutlet weak var customImage: UIImageView!
    @IBOutlet weak var customLabel: UILabel!
    @IBOutlet weak var customUIButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        _ = Bundle.main.loadNibNamed("CustomButton", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func createCustomButton(image : String, text : String) {
        
        self.backgroundColor = .clear
        
        let imageCustom = UIImage(named: image)
        customImage.image = imageCustom
        
        customLabel.text = text
        customLabel.font = UIFont.systemFont(ofSize: 12)
        customLabel.textColor = .white
        customLabel.backgroundColor = .clear
        customLabel.sizeToFit()
        customLabel.lineBreakMode = .byWordWrapping
        customLabel.numberOfLines = 0
    }
    
    @IBAction func loginButton(_ sender: Any) {
        print("button tapped")
    }
}
