//
//  Constrains.swift
//  TestApp
//
//  Created by Anton Hoang on 14/05/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation

//        customUIButton.translatesAutoresizingMaskIntoConstraints = false
//        customImage.translatesAutoresizingMaskIntoConstraints = false
//        customLabel.translatesAutoresizingMaskIntoConstraints = false
//
//        customImage.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
//        customImage.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
//
//        customLabel.topAnchor.constraint(equalTo: customImage.bottomAnchor).isActive = true
//        customLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
//        customLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
//        customLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true


//    func createCustomButton () {
//
//        let customFB : SocialNetwork = .facebook
//        let customGoogle : SocialNetwork = .google
//        let customTwitter : SocialNetwork = .twitter
//
//        let customViewFaceBookButton = Bundle.main.loadNibNamed("CustomButton", owner: self, options: nil)?.first as! CustomButton
//
//        let customViewGoogleButton = Bundle.main.loadNibNamed("CustomButton", owner: self, options: nil)?.first as! CustomButton
//
//        let customViewTwitterButton = Bundle.main.loadNibNamed("CustomButton", owner: self, options: nil)?.first as! CustomButton
//
//        customViewFaceBookButton.createCustomButton(image: customFB.imageName,
//                                                    text: customFB.title)
//
//        customViewGoogleButton.createCustomButton(image: customGoogle.imageName,
//                                                  text: customGoogle.title)
//
//        customViewTwitterButton.createCustomButton(image: customTwitter.imageName,
//                                                   text: customTwitter.title)
//
//
//        self.view.addSubview(customViewFaceBookButton)
//        self.view.addSubview(customViewGoogleButton)
//        self.view.addSubview(customViewTwitterButton)
//
//        customViewFaceBookButton.translatesAutoresizingMaskIntoConstraints = false
//        customViewGoogleButton.translatesAutoresizingMaskIntoConstraints = false
//        customViewTwitterButton.translatesAutoresizingMaskIntoConstraints = false
//
//        self.view.addConstraints([
//
//            customViewFaceBookButton.leftAnchor.constraint(equalTo: sendButton.leftAnchor),
//            customViewTwitterButton.rightAnchor.constraint(equalTo: sendButton.rightAnchor),
//            customViewFaceBookButton.rightAnchor.constraint(equalTo: customViewGoogleButton.leftAnchor),
//            customViewTwitterButton.leftAnchor.constraint(equalTo: customViewGoogleButton.rightAnchor),
//
//            customViewFaceBookButton.widthAnchor.constraint(equalTo: customViewGoogleButton.widthAnchor),
//            customViewGoogleButton.widthAnchor.constraint(equalTo: customViewTwitterButton.widthAnchor),
//            customViewFaceBookButton.heightAnchor.constraint(equalTo: customViewGoogleButton.heightAnchor),
//            customViewGoogleButton.heightAnchor.constraint(equalTo: customViewTwitterButton.heightAnchor),
//
//            customViewFaceBookButton.topAnchor.constraint(equalTo: customViewGoogleButton.topAnchor),
//            customViewGoogleButton.topAnchor.constraint(equalTo: customViewTwitterButton.topAnchor),
//            customViewTwitterButton.topAnchor.constraint(equalTo: sendButton.bottomAnchor, constant : 78),
//
//            customViewFaceBookButton.bottomAnchor.constraint(equalTo: customViewGoogleButton.bottomAnchor),
//            customViewGoogleButton.bottomAnchor.constraint(equalTo: customViewTwitterButton.bottomAnchor),
//        ])
//    }

//    private func setupConstraints() {
//
//        labelTitle.translatesAutoresizingMaskIntoConstraints = false
//        loginTextField.translatesAutoresizingMaskIntoConstraints = false
//        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
//        sendButton.translatesAutoresizingMaskIntoConstraints = false
//        recoveryPassButton.translatesAutoresizingMaskIntoConstraints = false
//        registrationButton.translatesAutoresizingMaskIntoConstraints = false
//
//        self.view.addConstraints([
//            labelTitle.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 65),
////            labelTitle.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 123),
//            labelTitle.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
//
//            loginTextField.topAnchor.constraint(equalTo: labelTitle.bottomAnchor, constant: 52),
//            loginTextField.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 60),
//            loginTextField.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
//
//            passwordTextField.topAnchor.constraint(equalTo: loginTextField.bottomAnchor, constant: 30),
//            passwordTextField.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 60),
//            passwordTextField.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
//
//            recoveryPassButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 21),
//            recoveryPassButton.rightAnchor.constraint(equalTo: loginTextField.rightAnchor),
//
//            sendButton.topAnchor.constraint(equalTo: recoveryPassButton.bottomAnchor),
//            sendButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant : 60),
//            sendButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
//
//            registrationButton.leftAnchor.constraint(equalTo: self.view.leftAnchor),
//            registrationButton.rightAnchor.constraint(equalTo: self.view.rightAnchor),
//            registrationButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant : -20)
//        ])
//    }


//    @IBAction func sendButtonAction(_ sender: Any) {
//
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let estimateVC = storyBoard.instantiateViewController(withIdentifier: "EstimateViewController")
//        self.navigationController?.pushViewController(estimateVC, animated: true)
//    }
//
//    @IBAction func recoveryPassButton(_ sender: Any) {
//    }
//
//    @IBAction func registrationButton(_ sender: Any) {
//    }
//
//    func labelConfigurator () {
//        labelTitle.text = "Calculator"
//        labelTitle.textColor = .white
//        labelTitle.font = UIFont.systemFont(ofSize: 25)
//        labelTitle.font = UIFont.boldSystemFont(ofSize: 25)
//    }
//}

