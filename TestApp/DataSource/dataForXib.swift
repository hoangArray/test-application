//
//  DataForXib.swift
//  TestApp
//
//  Created by Anton Hoang on 13/04/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation

enum SocialNetwork {
    case facebook
    case google
    case twitter
    
    var imageName: String {
        switch self {
        case .facebook: return "facebook"
        case .google:   return "google"
        case .twitter:  return "twitter"
        }
    }
    
    var title: String {
        switch self {
        case .facebook: return "Login with Facebook"
        case .google :  return "Login with Google"
        case .twitter : return "Login with Twitter"
        }
    }
}
