//
//  ViewController.swift
//  TestApp
//
//  Created by Anton Hoang on 08/04/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var loginTextField: TextFieldView!
    @IBOutlet weak var passwordTextField: TextFieldView!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var recoveryPassButton: UIButton!
    @IBOutlet weak var facebookButton: CustomButton!
    @IBOutlet weak var googleButton: CustomButton!
    @IBOutlet weak var twitterButton: CustomButton!
    @IBOutlet weak var registrationButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginTextField.textFieldConfigurator(image: "user", placeholder: "login", isSecure: false)
        passwordTextField.textFieldConfigurator(image: "password", placeholder: "password", isSecure: true)
        facebookButton.createCustomButton(image: SocialNetwork.facebook.imageName, text: SocialNetwork.facebook.title)
        googleButton.createCustomButton(image: SocialNetwork.google.imageName, text: SocialNetwork.google.title)
        twitterButton.createCustomButton(image: SocialNetwork.twitter.imageName, text: SocialNetwork.twitter.title)
        
        labelConfigurator()
        //KVO , textfield subclass ,layer frame
    }
    
    
    
    
    @IBAction func sendButtonAction(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let estimateVC = storyBoard.instantiateViewController(withIdentifier: "EstimateViewController")
        self.navigationController?.pushViewController(estimateVC, animated: true)
    }
    
    @IBAction func recoveryPassButton(_ sender: Any) {
    }
    
    @IBAction func registrationButton(_ sender: Any) {
    }
    
    func labelConfigurator () {
        labelTitle.text = "Calculator"
        labelTitle.textColor = .white
        labelTitle.font = UIFont.systemFont(ofSize: 25)
        labelTitle.font = UIFont.boldSystemFont(ofSize: 25)
    }
}
