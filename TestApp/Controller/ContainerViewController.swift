//
//  EstimateViewController.swift
//  TestApp
//
//  Created by Anton Hoang on 10/04/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    
    @IBOutlet weak var calculatorLabel: UILabel!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var viewContainer: UIView!
    
    private lazy var underlineView: UIView = {
        let result = UIView()
        result.backgroundColor = .red
        
        self.view.addSubview(result)
        result.translatesAutoresizingMaskIntoConstraints = false
        
        result.topAnchor.constraint(equalTo: segmentControl.bottomAnchor).isActive = true
        result.heightAnchor.constraint(equalToConstant: 3).isActive = true
        result.widthAnchor.constraint(equalTo: segmentControl.widthAnchor, multiplier: 1 / CGFloat(segmentControl.numberOfSegments)).isActive = true
        buttonBarLeftAnchor = result.leftAnchor.constraint(equalTo: segmentControl.leftAnchor)
        buttonBarLeftAnchor.isActive = true
        return result
    }()
    
    private var buttonBarLeftAnchor : NSLayoutConstraint!
    
    private let palindromeVC: EstimationViewController = {
        let sb = UIStoryboard(name: "Main", bundle: .main)
        let result = sb.instantiateViewController(withIdentifier: "EstimationViewController") as! EstimationViewController
        result.viewModel = PalindromeViewModel()
        return result
    }()
    
    private let factorialVC: EstimationViewController = {
        let sb = UIStoryboard(name: "Main", bundle: .main)
        let result = sb.instantiateViewController(withIdentifier: "EstimationViewController") as! EstimationViewController
        result.viewModel = FactorialViewModel()
        return result
    }()
    
    private let pairVC: PairsViewController = {
        let sb = UIStoryboard(name: "Main", bundle: .main)
        let result = sb.instantiateViewController(withIdentifier: "PairsViewController") as! PairsViewController
        return result
    }()
    
    
    private lazy var controllers: [UIViewController] = [self.palindromeVC, self.factorialVC, self.pairVC]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        self.selectedSegmentAtIndex(index: 0)
    }
    
    private func setupUI() {
        segmentControl.backgroundColor = .clear
        segmentControl.tintColor = .clear
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.lightGray,
                                               NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16)], for: .normal)
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16),
                                               NSAttributedString.Key.foregroundColor : UIColor.white], for: .selected)
        _ = underlineView
    }
    
    private func selectedSegmentAtIndex(index: Int) {
        self.children.forEach { self.removeChildViewController(childViewController: $0) }
        self.addViewControllerAsChildViewController(childViewController: self.controllers[index])
        
        let padding = underlineView.frame.width * CGFloat(self.segmentControl.selectedSegmentIndex)
        buttonBarLeftAnchor.constant = padding
    }
    
    @IBAction func switchAction(_ sender: UISegmentedControl) {
        self.selectedSegmentAtIndex(index: sender.selectedSegmentIndex)
    }
}

extension ContainerViewController {
    private func addViewControllerAsChildViewController(childViewController : UIViewController) {
        addChild(childViewController)
        view.addSubview(childViewController.view)
        childViewController.view.frame = viewContainer.bounds
        childViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        childViewController.didMove(toParent: self)
        
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        childViewController.view.topAnchor.constraint(equalTo: viewContainer.topAnchor).isActive = true
        childViewController.view.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        childViewController.view.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        childViewController.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }

    private func removeChildViewController(childViewController: UIViewController) {
        guard !self.children.isEmpty,
            self.children.contains(childViewController) else {
                return
        }
        childViewController.willMove(toParent: nil)
        childViewController.view.removeFromSuperview()
        childViewController.removeFromParent()
    }
}
