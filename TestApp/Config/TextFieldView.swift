//
//  TextFieldConfigurator.swift
//  TestApp
//
//  Created by Anton Hoang on 13/04/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit

class TextFieldView: UITextField {
    
    let bottomLine: CALayer = {
        let result = CALayer()
        result.borderColor = UIColor.white.cgColor
        result.borderWidth = 1
        return result
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        bottomLine.frame = CGRect(x: 0,
                                  y: self.frame.size.height - bottomLine.borderWidth,
                                  width: self.frame.size.width,
                                  height: bottomLine.borderWidth)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.layer.addSublayer(bottomLine)
    }

    func textFieldConfigurator (image : String, placeholder : String, isSecure: Bool) {
        
        let imageTextField = UIImageView(image: UIImage(named: image))
        imageTextField.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        imageTextField.tintColor = .white
        
        self.leftView = imageTextField
        self.leftView?.contentMode = .scaleAspectFit
        self.leftViewMode = .always
        self.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        self.tintColor = .white
        self.backgroundColor = .clear
        self.borderStyle = .none
        self.textColor = .white
        
        self.isSecureTextEntry = isSecure
    }
}
