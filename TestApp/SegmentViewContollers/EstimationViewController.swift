//
//  PalindromeViewController.swift
//  TestApp
//
//  Created by Anton Hoang on 16/04/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.

import UIKit

class EstimationViewController: UIViewController  {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var calculateButton: UIButton!
    @IBOutlet weak var resultLabel: UILabel!
    
    var viewModel : EstimationViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerLabel.text = "Enter the number"
        calculateButton.setTitle("Calculate", for: .normal)
        resultLabel.text = "Result"
        viewModel.delegate = self
        
        createReturnKey()
    }
    
    
    private func createReturnKey () {
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        
        toolBar.setItems([flexSpace, doneButton], animated: false)
        toolBar.sizeToFit()
        
        self.textField.inputAccessoryView = toolBar
        
    }
    
    @objc func doneButtonAction () {
        self.view.endEditing(true)
    }
    
    @IBAction func didSelectCalculate(_ sender: UIButton) {
        
        viewModel.didSelectCalculate(with: textField.text)
    }
}

extension EstimationViewController : EstimationViewModelDelegate {
    
    func resultTextDidUpdate(with text: String) {
        resultLabel.text = text
    }
}

protocol EstimationViewModelDelegate: class {
    func resultTextDidUpdate(with text: String)
}

class EstimationViewModel {
    
    var counter = Counter()

    var headerTitle: String
    var resultText: String {
        didSet {
            self.delegate?.resultTextDidUpdate(with: resultText)
        }
    }
    weak var delegate: EstimationViewModelDelegate?
    
    init() {
        headerTitle = "Enter the number"
        resultText = ""
    }
    
    func didSelectCalculate(with text: String?) {
        fatalError("Subclass should override")
    }
}

class FactorialViewModel: EstimationViewModel {
    
    override func didSelectCalculate(with text: String?) {
        if let text = text {
            if text.isEmpty { return }
            let intValue = Int(text)
            let result = counter.findSumOfDigits(n: intValue!)
            self.resultText = "\(result)"
        }
    }
}

class PalindromeViewModel: EstimationViewModel {
    
    override func didSelectCalculate(with text: String?) {
        if let text = text {
            if text.isEmpty { return }
            let intValue = Int(text)
            if counter.findPalindrome(number: intValue!) {
                resultText = "Palindrome"
            } else {
                resultText = "non palindrome"
            }
        }
    }
}

