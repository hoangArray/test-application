//
//  PairsViewController.swift
//  TestApp
//
//  Created by Anton Hoang on 16/04/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit

class PairsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    @IBOutlet weak var lableTitle: UILabel!
    @IBOutlet weak var findButton: UIButton!
    @IBOutlet weak var contentTableViewCell: UITableView!
    @IBOutlet weak var pairsTitle: UILabel!
    @IBOutlet weak var resultLabelText: UILabel!
    
    let identifier = "elements"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        contentTableViewCell.delegate = self
        contentTableViewCell.dataSource = self
    }
    @IBAction func findPairsButton(_ sender: Any) {
        let (min, max) = pairsArray.getLongestSublist()
        
        guard let minV = min,
              let maxV = max else { return }
        
        resultLabelText.text = "\(minV.firstElement, minV.secondElement)" +
                               "\(maxV.firstElement, maxV.secondElement)"
    }
    
    @IBAction func clearButton(_ sender: Any) {
        resultLabelText.text = "result"
    }
    private func setupUI () {
        self.lableTitle.text = "Get the longest sub-list of those pairs"
        self.lableTitle.lineBreakMode = .byWordWrapping
        self.lableTitle.numberOfLines = 0
        self.lableTitle.sizeToFit()
        self.findButton.setTitle("Find a list", for: .normal)
    }
    
    //MARK - create tableView and show array
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pairsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath)
        let item = pairsArray[indexPath.row]
    
        cell.textLabel?.text = "\(item.firstElement)" + "     " +
                               "\(item.secondElement)"
        return cell
    }
    
    let  pairsArray : [Pairs] = [Pairs(firstElement: 1, secondElement: 4),
                                 Pairs(firstElement: 2, secondElement: 5),
                                 Pairs(firstElement: 3, secondElement: 6),
                                 Pairs(firstElement: 4, secondElement: 7),
                                 Pairs(firstElement: 7, secondElement: 3),
                                 Pairs(firstElement: 4, secondElement: 6),
                                 Pairs(firstElement: 7, secondElement: 7),
                                 Pairs(firstElement: 8, secondElement: 4),
                                 Pairs(firstElement: 9, secondElement: 5),
                                 Pairs(firstElement: 10, secondElement: 6)]
}
